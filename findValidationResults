//****************************************************************************************************
//***
//*** API: 					ValidationApi 
//*** Method: 				findValidationResults(FindValidationResultRequest findValidationResultRequest)
//*** Returns: 				PagedResponse<ValidationResult>
//*** Description: 			Searches the validation results according to the given criteria in the FindValidationResultRequest object
//*** Required Parameters: 	a FindValidationResultRequest object
//*** Use Case: 			Find most recent validation job results of current asset.
//***
//****************************************************************************************************

//import the class to the FindValidationResultRequest object
//not necessary if using builders.get("FindValidationResultRequest")
import com.collibra.dgc.core.api.dto.validation.FindValidationResultRequest

//Defining the variables for the example:
//*** assetId - the id of the current Asset:
def assetId = itemV2.getId()

//Defining the findValidationResultRequest object
//*** Example A - by using builder()
findValidationResultRequest = FindValidationResultRequest.builder()
								.assetId(assetId)
								.mostRecentJob(true) 
								.build()

//Defining the findValidationResultRequest object
//*** Example B - by using constructor
findValidationResultRequest = new FindValidationResultRequest()
findValidationResultRequest.setAssetId(assetId)
findValidationResultRequest.setMostRecentJob(true) 

//Running to findValidationResults and saving it to a variable
validationResults = validationApi.findValidationResults(findValidationResultRequest)

//Converting the results to a List:
validationResultsList = validationResults.getResults()